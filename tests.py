import copy
import datetime
import os
from unittest import TestCase

from netticapy.api import DnsApi
from netticapy.api import DomainRecord
from netticapy.api import ServiceResult


def get_or_raise(name):
    """
    Tries to get an environment variable or an attribute of
    test_settings.py matching the given name. Raises RuntimeError
    on a miss.
    """
    try:
        import test_settings
    except ImportError:
        test_settings = None

    var = os.environ.get(name) or getattr(test_settings, name, None)
    if not var:
        msg = 'Cannot parse setting or env variable %s' % name
        raise RuntimeError(msg)
    return var


class DomainRecordTests(TestCase):

    def setUp(self):
        self.zone = get_or_raise("NETTICAPY_TEST_ZONE")
        self.host_name = get_or_raise("NETTICAPY_TEST_HOST_NAME")
        self.domain_record = DomainRecord(
            record_type="CNAME",
            domain_name=self.zone,
            host_name=self.host_name,
            priority=0,
            ttl=1,
            data="www.schneevonmorgen.com"
        )

    def test_as_soap_dict_1(self):
        """
        Keys must be correct and values must match.
        """
        rec = self.domain_record
        soap_dict = rec._as_soap_dict()

        self.assertEquals(soap_dict['DomainName'], rec.domain_name)
        self.assertEquals(soap_dict['HostName'], rec.host_name)
        self.assertEquals(soap_dict['RecordType'], rec.record_type)
        self.assertEquals(soap_dict['Data'], rec.data)
        self.assertEquals(soap_dict['TTL'], rec.ttl)
        self.assertEquals(soap_dict['Priority'], rec.priority)

    def test_eq_1(self):
        """
        domain name must be equal
        """
        other_rec = copy.copy(self.domain_record)
        other_rec.domain_name = 'whatever' + self.domain_record.domain_name
        self.assertNotEquals(self.domain_record, other_rec)

    def test_eq_2(self):
        """
        host name must be equal
        """
        other_rec = copy.copy(self.domain_record)
        other_rec.host_name = 'whatever' + self.domain_record.host_name
        self.assertNotEquals(self.domain_record, other_rec)

    def test_eq_3(self):
        """
        data must be equal
        """
        other_rec = copy.copy(self.domain_record)
        other_rec.data = 'whatever' + self.domain_record.data
        self.assertNotEquals(self.domain_record, other_rec)

    def test_eq_4(self):
        """
        record type must be equal
        """
        other_rec = copy.copy(self.domain_record)
        other_rec.record_type = 'whatever' + self.domain_record.record_type
        self.assertNotEquals(self.domain_record, other_rec)

    def test_eq_5(self):
        """
        ttl can differ to be equal
        """
        other_rec = copy.copy(self.domain_record)
        other_rec.ttl = self.domain_record.ttl + 10
        self.assertEquals(self.domain_record, other_rec)

    def test_eq_6(self):
        """
        priority can differ to be equal
        """
        other_rec = copy.copy(self.domain_record)
        other_rec.priority = self.domain_record.priority + 10
        self.assertEquals(self.domain_record, other_rec)


class DnsApiTests(TestCase):

    def setUp(self):
        self.zone = get_or_raise("NETTICAPY_TEST_ZONE")
        self.host_name = get_or_raise("NETTICAPY_TEST_HOST_NAME")
        self.username = get_or_raise("NETTICAPY_TEST_USERNAME")
        self.password = get_or_raise("NETTICAPY_TEST_PASSWORD")

    def test_get_service_info_1(self):
        """
        Test auth failure.
        """
        api = DnsApi('user_abc', 'pass_abc')

        with self.assertRaises(RuntimeError) as cm:
            api.get_service_info()

        self.assertTrue("401" in cm.exception.message)

    def test_get_service_info_2(self):
        """
        Test auth okay and ServiceResult gets returned.
        """
        api = DnsApi(self.username, self.password)
        service_result = api.get_service_info()

        self.assertTrue(isinstance(service_result, ServiceResult))
        self.assertTrue(isinstance(service_result.remaining_credits, int))
        self.assertTrue(isinstance(service_result.total_credits, int))
        self.assertTrue(isinstance(service_result.service_renewal_date,
                                   datetime.datetime))

    def test_list_zones_1(self):
        """
        Test zone listing returns a list of strings or an empty list.
        """
        api = DnsApi(self.username, self.password)
        zones = api.list_zones()

        self.assertTrue(isinstance(zones, list))
        for zone in zones:
            self.assertTrue(isinstance(zone, (str, unicode,)))

    def test_record_crud(self):
        """
        Creates, reads, updates and deletes a record.
        """
        api = DnsApi(self.username, self.password)
        domain_record = DomainRecord(
            record_type="CNAME",
            domain_name=self.zone,
            host_name=self.host_name,
            priority=0,
            ttl=1,
            data="www.schneevonmorgen.com"
        )

        # Step 0: Delete the test record in case it exists
        existing_record = api.read_record(
            domain_name=domain_record.domain_name,
            host_name=domain_record.host_name,
            data=domain_record.data,
            record_type=domain_record.record_type
        )
        if existing_record:
            api.delete_record(existing_record)

        # Step 1: Create
        api.create_record(domain_record)
        with self.assertRaises(RuntimeError) as cm:
            api.create_record(domain_record)
        self.assertTrue("431" in cm.exception.message)  # Already exists

        # Step 2: Read
        read_domain_record = api.read_record(
            domain_name=domain_record.domain_name,
            host_name=domain_record.host_name,
            data=domain_record.data,
            record_type=domain_record.record_type
        )
        self.assertEquals(read_domain_record, domain_record)

        # # Step 3: Update
        updated_domain_record = copy.copy(read_domain_record)
        updated_domain_record.data = "www.topfstedt.com"
        api.update_record(read_domain_record, updated_domain_record)
        read_domain_record = api.read_record(**updated_domain_record.__dict__)
        self.assertEquals(read_domain_record.data, "www.topfstedt.com")

        # Step 4: Delete
        api.delete_record(read_domain_record)
        self.assertIsNone(api.read_record(**read_domain_record.__dict__))

    def test_update_record_1(self):
        """
        Test that updating a non existing record fails.
        """
        api = DnsApi(self.username, self.password)
        old_domain_record = DomainRecord(
            record_type="CNAME",
            domain_name=self.zone,
            host_name=self.host_name,
            priority=0,
            ttl=1,
            data="www.schneevonmorgen.com"
        )
        new_domain_record = DomainRecord(
            record_type="A",
            domain_name=self.zone,
            host_name=self.host_name,
            priority=0,
            ttl=1,
            data="8.8.8.8"
        )
        with self.assertRaises(RuntimeError) as cm:
            api.update_record(old_domain_record, new_domain_record)
        self.assertTrue("404" in cm.exception.message)

    def test_list_domain_records_1(self):
        """
        Test listing domain records.
        """
        api = DnsApi(self.username, self.password)
        domain_records = api.list_domain_records(self.zone)

        self.assertTrue(hasattr(domain_records, 'next'))  # is a generator

        for domain_record in domain_records:
            self.assertTrue(isinstance(domain_record, DomainRecord))
