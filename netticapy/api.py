import base64
import collections
import urllib
import urllib2

from suds.client import Client


class DomainRecord(object):
    DEFAULT_TTL = 0
    DEFAULT_PRIORITY = 0

    def __init__(self, domain_name, host_name, data, record_type,
                 ttl=DEFAULT_TTL, priority=DEFAULT_PRIORITY):
        self.domain_name = domain_name
        self.host_name = host_name
        self.data = data
        self.record_type = record_type
        self.ttl = ttl
        self.priority = priority

    def __eq__(self, other):
        """
        Override equals - the uniqueness of a domain record is defined
        by the domain name, the host name, the data and the record type.
        """
        return all([
            self.domain_name == other.domain_name,
            self.host_name == other.host_name,
            self.data == other.data,
            self.record_type == other.record_type,
        ])

    def _as_soap_dict(self):
        """
        Returns the object as a dict that is compatible with the
        complex type DomainRecord defined by netticas SOAP API.
        """
        return {
            "DomainName": self.domain_name,
            "HostName": self.host_name,
            "RecordType": self.record_type,
            "Data": self.data,
            "TTL": self.ttl,
            "Priority": self.priority,
        }


ServiceResult = collections.namedtuple(
    'ServiceResult',
    'remaining_credits, total_credits, service_renewal_date'
)


class DnsApi(object):
    """
    A python wrapper for Nettica's SOAP API.
    """
    endpoint = "https://www.nettica.com/DNS/DnsApi.asmx?WSDL"

    def __init__(self, username, password):
        self.username = username
        self.encoded_password = base64.b64encode(password)

    def _get_client(self):
        return Client(self.endpoint)

    def get_service_info(self):
        """
        Returns the service info as a named tuple.
        Raises a RuntimeError on error.
        """
        client = self._get_client()
        response = client.service.GetServiceInfo(self.username,
                                                 self.encoded_password)

        if response.Result.Status != 200:
            msg = "%d: %s" % (response.Result.Status,
                              response.Result.Description)
            raise RuntimeError(msg)
        else:
            return ServiceResult(
                remaining_credits=response.RemainingCredits,
                total_credits=response.TotalCredits,
                service_renewal_date=response.ServiceRenewalDate,
            )

    def list_domain_records(self, zone):
        """
        Returns all domain records for the given domain name as
        named tuples.
        Raises a RuntimeError on error.
        """
        client = self._get_client()
        response = client.service.ListDomain(self.username,
                                             self.encoded_password,
                                             zone)

        if response.Result.Status != 200:
            msg = "%d: %s" % (response.Status, response.Description)
            raise RuntimeError(msg)
        else:
            for domain_record in response.Record[0]:
                yield DomainRecord(
                    record_type=domain_record.RecordType,
                    domain_name=domain_record.DomainName,
                    host_name=domain_record.HostName,
                    priority=domain_record.Priority,
                    ttl=domain_record.TTL,
                    data=domain_record.Data,
                )

    def list_zones(self):
        """
        Returns the zones as a list of strings.
        Raises a RuntimeError on error.
        """
        client = self._get_client()
        response = client.service.ListZones(self.username,
                                            self.encoded_password)

        if response.Result.Status != 200:
            msg = "%d: %s" % (response.Status, response.Description)
            raise RuntimeError(msg)
        else:
            return response.Zone[0]

    def create_record(self, domain_record):
        """
        Creates a domain record.
        Raises a RuntimeError on error.
        """
        client = self._get_client()
        response = client.service.AddRecord(
            self.username,
            self.encoded_password,
            domain_record._as_soap_dict(),
        )

        if response.Status != 200:
            msg = "%d: %s" % (response.Status, response.Description)
            raise RuntimeError(msg)

    def filter_records(self, record_type=None, domain_name=None,
                       host_name=None, priority=None, ttl=None, data=None):
        """
        Reads a domain record and returns all matches, if any.
        """

        for rec in self.list_domain_records(domain_name):
            if rec.host_name != host_name:
                continue
            if record_type is not None and rec.record_type != record_type:
                continue
            if data is not None and rec.data != data:
                continue
            yield rec

    def read_record(self, domain_name, host_name, data, record_type, **kwargs):
        """
        Reads and returns a domain record matching the given arguments.
        """
        spec_rec = DomainRecord(domain_name, host_name, data, record_type)
        for domain_rec in self.list_domain_records(domain_name):
            if domain_rec == spec_rec:
                return domain_rec

    def update_record(self, old_domain_record, new_domain_record):
        """
        Updates an old domain record with a new domain record.
        Raises a RuntimeError on error.
        """
        client = self._get_client()
        response = client.service.UpdateRecord(
            self.username,
            self.encoded_password,
            old_domain_record._as_soap_dict(),
            new_domain_record._as_soap_dict(),
        )
        if response.Status != 200:
            msg = "%d: %s" % (response.Status, response.Description)
            raise RuntimeError(msg)

    def delete_record(self, domain_record):
        """
        Deletes a domain record.
        Raises a RuntimeError on error.
        """
        client = self._get_client()
        response = client.service.DeleteRecord(
            self.username,
            self.encoded_password,
            domain_record._as_soap_dict(),
        )
        if response.Status != 200:
            msg = "%d: %s" % (response.Status, response.Description)
            raise RuntimeError(msg)


class UpdateApi(object):
    """
    A python wrapper for Nettica's Update API.
    """
    endpoint = "https://www.nettica.com/Domain/Update.aspx"

    def __init__(self, username, password):
        self.username = username
        self.encoded_password = base64.b64encode(password)

    def update_ip_address(self, domain_name, new_ip_address):
        """
        Updates thte ip address of a fully qualified domain name.
        """
        data = urllib.urlencode({
            "U": self.username,
            "P": self.encoded_password,
            "FQDN": domain_name,
            "N": new_ip_address,
        })
        url = "%s?%s" % (self.endpoint, data)

        urllib2.urlopen(url)
